## Fedora Fastest Mirror Finder

<p>
This is a program that finds the fastest mirror(downloader) for Fedora.
It will search through all the possibilities and check their speed so may take a
while to run.
</p>

### A Simple Mirror Finder For Fedora Linux
- Version 0.2
- stable

### Not That
- <u> please before run this program create backup on repository this patch </u> , <u>`mkdir ~/Backup_Repository && cp /etc/yum.repos.d/* ~/Backup_Repository` </u>


####  Dependencies used:
- <u>Library qt-base</u>
- <u>qt5-qtbase-devel</u>
- <u>make</u>

<u> If you do not have these installed you can install them using ,</u> <u> `sudo dnf install qt5-qtbase` </u>, <u> `sudo dnf install qt5-qtbase-devel` </u> and <u> `sudo dnf install make` </u> <u> respectively.</u>

##### Generating a `MakeFile`
- <u>clone the files from this repos first:`git clone https://gitlab.com/Alone_Ghost/finde-best-mirror-fedora.git` </u>
- <u>`cd finde-best-mirror-fedora` </u>
- <u> and run this command</u> `qmake-qt5`


###### How to Compile:
- <u>Open your preferred terminal and go to patch clone file : if you clone into home `cd finde-best-mirror-fedora` </u>
- <u>Run the command </u> `make`
- <u>Wait for it to compile!</u>

#### To run:
- <u>Run the command </u> `chmod +x MirroFinder`
- <u>Run </u> `sudo ./MirroFinder`
- <b>You should only need to run this once to find your fastest mirror. </b>

### Note:
- <u> When running make sure to use sudo. </u>
- <u> The process can take a while, so grab a cup of coffee and relax, you'll see a "finish" output when done, you can use your PC.</u>

### The Future
#### To Do [2/6]

- `[X]fedora.repo`
- `[X]fedora-updates.repo`
- `[]rpmfusion-free.repo`
- `[]rpmfusion-free-updates.repo`
- `[]fedora-modular.repo`
- `[]fedora-updates-modular.repo`
</p>

### Summary:
- `git clone https://gitlab.com/Alone_Ghost/finde-best-mirror-fedora.git`
- `cd finde-best-mirror-fedora`
- `qmake-qt5`
- `make`
- `chmod +x MirrorFinder`
- `sudo ./MirrorFinder`

##### This Software Only Works for these Repositories :

<table style="width:100%">
  <tr>
    <th>Repository</th>
    <th>Repo detail</th>
  </tr>
  <tr>
    <td>Fedora</td>
    <td>Fedora.repo</td>
  </tr>
   <tr>  
    <td>Fedora-updates</td>
    <td>Fedora-updates.repo</td>
  </tr>
</table>

<P>
<b>Contact Details:</b>

`recive_mail_gitlab@riseup.net`
</p>
